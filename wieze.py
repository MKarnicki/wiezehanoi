def WiezeHanoi(n, zabierany, docelowy, posredni):
    if n == 0:
        return
    WiezeHanoi(n - 1, zabierany, posredni, docelowy)
    podniesKlocek(zabierany)
    odlozKlocek(docelowy)
    licznik(zabierany,docelowy)
    print(f'Ruch numer', counter)
    WiezeHanoi(n - 1, posredni, docelowy, zabierany)
#https://www.geeksforgeeks.org/c-program-for-tower-of-hanoi/
def rysowanie(columna,columnb,columnc):
    for i in range (len(columna)):
        if columna[i] == 0:
            spacjea = " "*(len(columna)+1)
            wynika=(spacjea + "|"+ spacjea)
        else:
            spacjea = " " * ((len(columna)-columna[i])+1)
            gwiazdkia = "▇"*((columna[i]*2)+1)
            wynika=(spacjea + gwiazdkia + spacjea)
        if columnb[i] == 0:
            spacjeb = " "*(len(columnb)+1)
            wynikb=(spacjeb + "|" + spacjeb)
        else:
            spacjeb = " " * ((len(columnb)-columnb[i])+1)
            gwiazdkib = "▇"*((columnb[i]*2)+1)
            wynikb=(spacjeb + gwiazdkib + spacjeb)
        if columnc[i] == 0:
            spacjec = " " * (len(columnc) + 1)
            wynikc=(spacjec + "|" + spacjec)
        else:
            spacjec = " " * ((len(columnc) - columnc[i]) + 1)
            gwiazdkic = "▇" * ((columnc[i] * 2) + 1)
            wynikc = (spacjec + gwiazdkic + spacjec)
        print(wynika+wynikb+wynikc)
def podniesKlocek(column):
    for i in range(wielkosc):
        if column[i] != 0:
            global storage
            storage = column[i]
            column[i] = 0

            break
        elif i == wielkosc-1:
            print("Wieza jest pusta")
            break

def odlozKlocek(column):
    for i in range(wielkosc):
        if column[i] !=0:
            global storage

            if column[i] - storage > 0:
                column[i-1] = storage
                storage = 0
                rysowanie(col_a, col_b, col_c)
                break
            else:
                print("nielegalny ruch")
                break
        elif i == wielkosc-1:
            column[wielkosc-1]=storage
            storage = 0
            rysowanie(col_a, col_b, col_c)
            print(f'\n')



def licznik(a,b):
    if a!=b:
        global counter
        counter+=1
    else:
        print("Odlozyles klocek na ta sama wieze, nie policzymy ci tego ruchu :) ")

finished = True
storage = 0
counter = 0
wielkosc = 0
col_a = []
col_b = []
col_c = []
while finished:
    print(f'\n [0] - wyjdz z gry \n [1] - rozegraj partie \n [2] - obejzyj jak bot gra za ciebie')
    menu=input()
    if menu == '0':
        break
    elif menu == '1':
        finished = False
    elif menu == '2':
        while wielkosc < 1:
            wielkosc = int(input("Podaj wielkość wież: "))
        if len(col_a) < 1:
            for i in range(wielkosc):
                col_a.append(i + 1)
                col_b.append(0)
                col_c.append(0)
            rysowanie(col_a, col_b, col_c)
            print("Ruch numer 0")
        WiezeHanoi(wielkosc, col_a, col_c, col_b)
else:
    while not finished:
        menu = 3
        selected = False
        while not selected:
            while wielkosc < 1:
                wielkosc = int(input("Podaj wielkość wież: "))
            if len(col_a) <1:
                for i in range(wielkosc):
                    col_a.append(i + 1)
                    col_b.append(0)
                    col_c.append(0)
                rysowanie(col_a, col_b, col_c)
            wyborPodnoszenia = input("Z ktorej wiezy chcesz przesunac klocek: ")
            if wyborPodnoszenia == '1':
                selected = True
                podniesKlocek(col_a)
            elif wyborPodnoszenia == '2':
                selected = True
                podniesKlocek(col_b)
            elif wyborPodnoszenia == '3':
                selected = True
                podniesKlocek(col_c)
            elif wyborPodnoszenia == '0':
                finished = True
                break
        print("Ilosc ruchow: ", counter)
        while storage != 0:
            wyborOdlozenia = input("Na ktora wieze chcesz przesunac klocek: ")
            if wyborOdlozenia == '1':
                odlozKlocek(col_a)
                selected = False
                licznik(wyborOdlozenia,wyborPodnoszenia)
            elif wyborOdlozenia == '2':
                odlozKlocek(col_b)
                selected = False
                licznik(wyborOdlozenia, wyborPodnoszenia)
            elif wyborOdlozenia == '3':
                odlozKlocek(col_c)
                selected = False
                licznik(wyborOdlozenia, wyborPodnoszenia)
            elif wyborOdlozenia == '0':
                finished = True
                break

        print("Ilosc ruchow: ", counter)
print("Dziekuje za gre")
if storage !=0:
    print(f'Ale prosimy o oddanie klocka >:( ')